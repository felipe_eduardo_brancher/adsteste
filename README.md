#Projeto de exemplo com JUnit, JaCoCo e Sonar
------------------------------------------------------------

## Configura��o de proxy no SENAI
- git config --global http.proxy 10.3.79.253:3128
- git config --global https.proxy 10.3.79.253:3128

## Sonar
- Baixar o SonarQube: http://www.sonarqube.org/downloads/
- Descompactar e iniciar em <sonar>/bin/<plataforma>/<sonar | StartSonar> (por padr�o usa a porta 9000)
- caso n�o possua, colocar o arquivo settings.xml no diret�rio .m2. Exemplo: C:\Users\dieison.grumovski\.m2
- Rodar o build do projeto: mvn clean install sonar:sonar ou pelo proprio eclipse
- Acessar http://localhost:9000