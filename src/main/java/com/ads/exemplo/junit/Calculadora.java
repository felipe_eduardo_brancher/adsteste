package com.ads.exemplo.junit;

public class Calculadora implements ICalculadora {

	/**
	* Adição
	*/
	@Override
	public int somar(int a, int b) {
		return a + b;
	}

	/**
	* Subtração
	*/
	@Override
	public int subtrair(int a, int b) {
		return a - b;
	}

	/**
	* Multiplicação
	*/
	@Override
	public int multiplicar(int a, int b) {
		return a * b;
	}

	/**
	* Divis�o
	*/
	@Override
	public int dividir(int a, int b) throws Exception {
		if (b == 0) {
			throw new Exception("Não pode dividir por zero");
		}
		return a / b;
	}
	
	/**
	* Exponenciação
	*/
	public int expoente(int a, int b) {
		return a ^ b;
	}
	
	public int mod(int a, int b) {
		return a % b;
	}

	/**
	* Igualdade
	*/
	@Override
	public boolean validarNumerosIguais(int a, int b) {
		return a == b;
	}
}
